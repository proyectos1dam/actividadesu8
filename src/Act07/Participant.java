package Act07;

import java.sql.SQLOutput;
import java.util.HashSet;
import java.util.Objects;

public class Participant {
    private String nom;
    private String dni;
    private double temps;

    public Participant(String nom, String dni, double temps) {
        this.nom = nom;
        this.dni = dni;
        this.temps = temps;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participant that = (Participant) o;
        return Objects.equals(dni, that.dni);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dni);
    }

    @Override
    public String toString() {
        return  "Nom: " + nom +
                ", DNI: " + dni +
                ", Temps: " + temps;
    }

    public static void main(String[] args) {
        HashSet<Participant> llista1 = new HashSet<>();
        HashSet<Participant> llista2 = new HashSet<>();

        Participant p1 = new Participant("Pepe González", "20051883T", 16.90);
        Participant p2 = new Participant("Paco Jover", "45454141T", 19.90);
        Participant p3 = new Participant("Joaquin Rodríguez", "00001111C", 16.70);
        Participant p4 = new Participant("Javier López", "20445666P", 22.44);
        Participant p5 = new Participant("José Martínez", "25656563T", 14.01);
        Participant p6 = new Participant("Pepe González", "20051883T",18.22);

        llista1.add(p1);
        llista1.add(p2);
        llista1.add(p3);

        llista2.add(p4);
        llista2.add(p5);
        llista2.add(p6);
        System.out.println("Prova 1");
        for (Participant part: llista1) {
            System.out.println(part.toString());
        }
        System.out.println();
        System.out.println("Prova 2");
        for (Participant part: llista2) {
            System.out.println(part.toString());
        }
        System.out.println();
        System.out.println("Participantes únicos");
        HashSet<Participant> todos = new HashSet<>();
        todos.addAll(llista1);
        todos.addAll(llista2);
        for (Participant part: todos) {
            System.out.println(part.toString());
        }
    }
}

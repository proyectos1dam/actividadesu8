package Act08;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ForoInteligente extends Foro{
    private Set<String> palabrasProhibidas;
    protected Set<String> spam;

    public ForoInteligente(String nombre, String... palabrasProhibidas) {
        super(nombre);
        this.palabrasProhibidas = new HashSet<>(List.of(palabrasProhibidas));
        this.spam = new HashSet<>();

    }

    @Override
    public boolean registrarEntrada(String descrip) {
        for (String element: palabrasProhibidas) {
            if(descrip.contains(element)){
                this.spam.add(descrip);
               return false;
            }
        }
        lista.add(new Entrada(descrip));
        return true;
    }

    @Override
    public int spams() {
        return super.spams() + spam.size();
    }
}

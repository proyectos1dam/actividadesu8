package Act08;

import java.time.LocalDateTime;

public class Entrada {
    private String descripcion;
    private LocalDateTime fecha;

    public Entrada(String descripcion) {
        this.descripcion = descripcion;
        this.fecha = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "Fecha: " + this.fecha +" , Entrada: " +
                this.descripcion;
    }
}

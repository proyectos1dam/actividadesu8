package Act08;

import java.util.LinkedList;

public class TestForo {
    public static void main(String[] args) {
        Foro basico1 = new Foro("ForoCoches");

        ForoInteligente inteligente1 = new ForoInteligente("FranceFootball");

        ForoInteligente inteligente2 = new ForoInteligente("ResourceGold","ataque","ataque","foro");

        LinkedList<Foro> todosForos = new LinkedList<>();

        todosForos.add(basico1);
        todosForos.add(inteligente1);
        todosForos.add(inteligente2);

        for (Foro foro: todosForos){
            foro.registrarEntrada("¿Habéis utilizado el servicio de Instagram, ayer me saltó un error en el servicio?");
            foro.registrarEntrada("Los de thepiratebay informaron que podría haber sido un ataque");
            foro.registrarEntrada("Algunos usuarios compartieron la publicación en facebook junto con una foto en la que aparecían los causantes del problema. ¡Era un error de programación!");
        }

        for (Foro foro: todosForos){
            int spam = foro.spams();
            System.out.println();
            System.out.println("Foro "+ foro.nombre);
            foro.mostrarEntradas();
            System.out.println("Sucesos de Spam: " + spam);
        }

    }
}

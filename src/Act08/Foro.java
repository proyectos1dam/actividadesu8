package Act08;

import java.util.ArrayList;
import java.util.List;

public class Foro {
    protected String nombre;
    protected List<Entrada> lista;

    public Foro(String nombre) {
        this.nombre = nombre;
        this.lista = new ArrayList<>();
    }

    public boolean registrarEntrada(String descrip){
        Entrada entrada = new Entrada(descrip);
        this.lista.add(entrada);
        return true;
    }

    public void mostrarEntradas(){
        for (Entrada e: lista){
            System.out.println(e.toString());
        }

    }
    public  int spams(){
        return 0;
    }
}

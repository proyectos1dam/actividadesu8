package Act05;

public class Pelicula extends Produccion{
    private String actor;
    private String actriz;

    public Pelicula(String actor, String actriz, String titulo, Formato formato, int duracion, Data fechaLanzamiento) {
        super(titulo, formato, duracion, fechaLanzamiento,12);
        this.actor = actor;
        this.actriz = actriz;
    }

    public Pelicula(String actor, String actriz, String titulo, Formato formato, Data fechaLanzamiento) {
        super(titulo, formato,4800, fechaLanzamiento,12);
        this.actor = actor;
        this.actriz = actriz;
    }



    @Override
    public String toString() {
        return super.toString() + " Actor: " + this.actor + ", Actriz: " + this.actriz;
    }

    @Override
    public void mostrarDetalle() {
        super.mostrarDetalle();
        System.out.println("Duraciï¿½n: " + getDuracionHorasMinutosSegundos());
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
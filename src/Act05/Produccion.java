package Act05;

import java.util.Objects;

public class Produccion {

    private String titulo;
    private Formato formato;
    protected int duracion;
    private String descripcion;
    private Data fechaLanzamiento;
    protected double precio;

    public Produccion() {
    }

    public Produccion(String titulo, Formato formato, int duracion,  Data fechaLanzamiento, double precio) {
        this.titulo = titulo;
        this.formato = formato;
        this.duracion = duracion;
        this.descripcion = "Lorem ipsum Lorem ipsum Lorem ipsum";
        this.fechaLanzamiento = fechaLanzamiento;
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Título: " + titulo + ", formato: " + formato + ", duración: " + duracion ;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void mostrarDetalle(){
        System.out.println(this.titulo + " (" + this.fechaLanzamiento.getAny() + ")");
        System.out.println("Descripción: " + this.descripcion);
    }

    protected String getDuracionHorasMinutosSegundos(){
        String durada = "";
        int horas = this.duracion / 3600;
        int minutos = (this.duracion % 3600) / 60;
        int segundos = (this.duracion % 3600 % 60);
        durada = horas + " h "+ minutos + " min " + segundos + " segs";
        return durada;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produccion that = (Produccion) o;
        return duracion == that.duracion && Objects.equals(titulo, that.titulo) && formato == that.formato && Objects.equals(descripcion, that.descripcion) && Objects.equals(fechaLanzamiento, that.fechaLanzamiento);
    }

}
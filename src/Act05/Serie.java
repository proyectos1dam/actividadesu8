package Act05;

public class Serie extends Produccion{
    private int temporada;
    private int capitulos;

    public Serie(int temporada, int capitulos, String titulo, Formato formato, Data fechaLanzamiento) {
        super(titulo, formato, 2400, fechaLanzamiento,24);
        this.temporada = temporada;
        this.capitulos = capitulos;
    }

    @Override
    public String toString() {
        return super.toString() + ", Temporada: " + this.temporada + ", Capitulos: " + this.capitulos;
    }

    @Override
    public void mostrarDetalle() {
        super.mostrarDetalle();
        System.out.println("Temporada " + this.temporada + " - Capítulos: " + this.capitulos);
        System.out.println("Duración: " + getDuracionHorasMinutosSegundos());
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
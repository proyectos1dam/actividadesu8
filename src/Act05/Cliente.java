package Act05;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
    private Data fechaNacimiento;
    private String email;
    private String nombre;
    private String apellidos;
    private String dni;
    protected List<Produccion> compras;

    public Cliente(Data fechaNacimiento, String email, String nombre, String apellidos, String dni) {
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.compras = new ArrayList<>();
    }

    public void comprar(Produccion prod){
        for (Produccion element: compras){
            if (element.equals(prod)){
                this.compras.remove(prod);
                break;
            }

        }
        this.compras.add(prod);
    }

    public void mostrarCompras(){
        for (Produccion element: compras){
            System.out.println(element.toString());
        }
        System.out.print("Total gastado: " + valorCompras() + "€");
        System.out.println();
    }

    public double valorCompras(){
        double x = 0;
        for (Produccion element: compras){
            x += element.precio;
        }
        return x;
    }

    @Override
    public String toString() {
        return "Cliente: " + nombre + " " + apellidos + " | Compras("+ compras.size() +")";
    }
}

package Act05;

public enum Formato {
    MPG{
        @Override
        public String toString() {
            return "MPG";
        }

    },AVI{
        @Override
        public String toString() {
            return "AVI";
        }

    },MOV{
        @Override
        public String toString() {
            return "MOV";
        }

    },FLV{
        @Override
        public String toString() {
            return "FLV";
        }

    };
}

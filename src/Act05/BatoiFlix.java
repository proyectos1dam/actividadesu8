package Act05;

public class BatoiFlix {
        public static void main(String[] args) {

            Data fecha1 = new Data(1, 10, 2020);
            Pelicula pel1 = new Pelicula("Tom Hanks", "Carolina Betller", "News of the world", Formato.AVI, 7200, fecha1);

            Data fecha2 = new Data(4,11, 2021);
            Documental doc1 = new Documental("Enrique Juncosa", "Movimiento Hippie", "Dream Songs", Formato.MPG, 2400, fecha2);

            Data fecha3 = new Data(10, 12, 2001);
            Serie serie1 = new Serie(1, 20, "El hacker", Formato.FLV, fecha3);

            Cliente cli1 = new Cliente(new Data(12,12,1993),"MGarcia@gmail.com","María","García Flint","11111111A");
            Cliente cli2 = new Cliente(new Data(07,8,1984),"jupermar@gmail.com","Juan","Pérez Martín","22222222B");
            Cliente cli3 = new Cliente(new Data(23,1,2000),"flipflop@gmail.com","Elena","Ramírez Fuentes","33333333C");

            cli1.comprar(pel1);
            cli1.comprar(doc1);
            cli1.comprar(serie1);
            System.out.println(cli1.toString());
            cli1.mostrarCompras();
            System.out.println();

            cli2.comprar(doc1);
            System.out.println(cli2.toString());
            cli2.mostrarCompras();
            System.out.println();

            cli3.comprar(pel1);
            System.out.println(cli3.toString());
            cli3.mostrarCompras();
        }
    }

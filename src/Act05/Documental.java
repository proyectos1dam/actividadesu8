package Act05;

public class Documental extends Produccion{
    private String investigador;
    private String tema;

    public Documental(String investigador, String tema, String titulo, Formato formato, int duracion, Data fechaLanzamiento) {
        super(titulo, formato, duracion, fechaLanzamiento,15);
        this.investigador = investigador;
        this.tema = tema;
    }

    @Override
    public String toString() {
        return super.toString() + ", Investigador: " + this.investigador + ", Tema: " + this.tema;
    }

    @Override
    public void mostrarDetalle() {
        super.mostrarDetalle();
        System.out.println("Investigador: " + this.investigador);
        System.out.println("Duración: " + getDuracionHorasMinutosSegundos());
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}

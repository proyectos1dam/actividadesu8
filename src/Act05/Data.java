package Act05;

import java.util.StringTokenizer;

public class Data {
    private int dia;
    private int mes;
    private int any;
    final static int DIASBISIESTO = 366;
    final static int DIASNOBISIESTO = 365;
    final static int MESCON31 = 31;
    final static int MESCON30 = 30;
    final static int FEBRERONOB = 28;
    final static int FEBREROB = 29;
    final static int DIESSETMANA = 7;

    private static final String[] DIES_TEXT = new String[]{"diumenge", "dilluns", "dimarts", "dimecres", "dijous", "divendres",
            "dissabte"};

    private static final String[] MESOS_TEXT = new String[]{"gener", "febrer", "marÃ§", "abril", "maig", "juny",
            "juliol", "agost", "setembre", "octubre", "novembre", "desembre"};

    /**
     * Constructor por defecto Inicializa una fecha a dia 1-1-1970
     */
    public Data() {
        this.dia = 1;
        this.mes = 1;
        this.any = 1070;
    }

    /**
     * Inicializa la fecha
     *
     * @param dia de la semana
     * @param mes del aÃ±o
     * @param any
     */
    public Data(int dia, int mes, int any) {
        this.dia = dia;
        this.mes = mes;
        this.any = any;
    }

    /**
     * Inicializa la fecha a partir de otra pasada en formato String dd/mm/yyyy
     *
     * Deberemos trocearlas de forma que asignemos el dÃ­a mÃ©s y aÃ±o a cada uno
     * de los atributoe Tienes que utilizar las funciones de *String o consultar
     * la documentaciÃ³n oficial y hacerlo OBLIGATORIAMENTE con la clase
     * StringTokenizer. Si el formato recibido no es el correcto, crearÃ¡ la
     * fecha por defecto.

    public Data(String data) {
        StringTokenizer st = new StringTokenizer(data, "/");
        while (st.hasMoreTokens()) {
            this.dia = Integer.parseInt(st.nextToken());
            this.mes = Integer.parseInt(st.nextToken());
            this.any = Integer.parseInt(st.nextToken());
        }
    }

    /**
     * Modifica la fecha actual a partir de los datos pasados como argumento
     */
    public void set(int dia, int mes, int anyo) {
        this.dia = dia;
        this.mes = mes;
        this.any = anyo;
    }

    /**
     * Obtiene una fecha con los mismos atributos que la fecha actual
     *
     * @return
     */
    public Data clone() {
        Data copia = new Data(this.dia, this.mes, this.any);
        return copia;
    }

    /**
     * Devuelve el dÃ­a de la semana que representa por la Fecha actual
     *
     * @return @dia
     */
    public int getDia() {
        return this.dia;
    }

    /**
     * Devuelve el mes que representa la Fecha actual
     *
     * @return @mes
     */
    public int getMes() {
        return this.mes;
    }

    /**
     * Devuelve el aÃ±o que representa la Fecha actual
     *
     * @return @mes
     */
    public int getAny() {
        return this.any;
    }

    /**
     * Muestra por pantalla la fecha en formato espaÃ±ol dd-mm-yyyy
     */
    public void mostrarEnFormatES() {
        System.out.printf("%02d-%02d-%04d\n", this.dia, this.mes, this.any);
    }

    /**
     * Muestra por pantalla la fecha en formato inglÃ©s yyyy-mm-dd
     */
    public void mostrarEnFormatGB() {
        System.out.printf("%04d-%02d-%02d\n", this.any, this.mes, this.dia);
    }

    /**
     * Muestra por pantalla la fecha en formato texto dd-mm-yyyy Ej. 1 enero de
     * 1970
     */
    public void mostrarEnFormatText() {
        System.out.printf("%02d-%s-%04d\n", this.dia, getMesEnFormatText(), this.any);
    }

    /**
     * Retorna un booleano indicando si la fecha del objeto es igual a la fecha
     * pasada como argumento
     *
     * @return boolean
     */
    public boolean isIgual(Data otraFecha) {
        return this.dia == otraFecha.getDia() && this.mes == otraFecha.mes && this.any == otraFecha.any;
    }

    /**
     * Retorna un String que representa el dia de la setmana en format text
     * (dilluns, dimarts, dimecres, dijous, divendres, dissabte, diumenge).
     * L'algorisme de resoluciÃ³ d'aquest mÃ¨tode es troba al enunciat.
     *
     * @return String
     */
    public String getDiaSetmana() {
        String dia = "";
        int diesTrans = getDiesTranscorregutsOrigen();
        int diaNum = diesTrans % DIESSETMANA;
        switch (diaNum) {
            case 0 ->
                    dia = DIES_TEXT[0];
            case 1 ->
                    dia = DIES_TEXT[1];
            case 2 ->
                    dia = DIES_TEXT[2];
            case 3 ->
                    dia = DIES_TEXT[3];
            case 4 ->
                    dia = DIES_TEXT[4];
            case 5 ->
                    dia = DIES_TEXT[5];
            case 6 ->
                    dia = DIES_TEXT[6];
        }

        return dia;
    }

    /**
     * Retorna un booleÃ  indicant si la data representada per l'objecte actual
     * Ã©s festiva. Es considerarÃ  festiu si el dia de la setmana Ã©s dissabte o
     * diumenge
     *
     * @return boolean
     */
    public boolean isFestiu() {
        String dia = "";
        dia = getDiaSetmana();
        if (dia == DIES_TEXT[0] || dia == DIES_TEXT[6]) {
            return true;
        }
        return false;
    }

    /**
     * Retorna el nÃºmero de la setmana dins de l'any actual. Es considera una
     * setmana l'interval de dates entre una data que siga dilluns i la segÃ¼ent
     * data en ordre cronolÃ²gic que siga diumenge. TambÃ© es comptabilitza com a
     * setmana tant la primera setmana de l'any com l'Ãºltima (inclusivament en
     * aquells anys en quÃ¨ la primera i/o Ãºltima setmana no contÃ© set dies en
     * total).
     *
     * @return int dia semana
     */
    public int getNumeroSetmana() {
        int diesTrans = getDiesTranscorregutsEnAny();
        int numSetmanes = diesTrans / DIESSETMANA;
        int indiceDia = diesTrans % DIESSETMANA;
        if (diesTrans < DIESSETMANA) {
            if(indiceDia != 0){
                numSetmanes++;
            }

        }
        return numSetmanes;
    }

    /**
     * Retorna un nou objecte de tipus data que representa la data resultant
     * d'afegir el nombre de dies passats com a argument a la data que
     * representa l'objecte actual. Haurem de tindre en compte els dies que tÃ©
     * el mes actual i si l'any Ã©s de traspÃ s (bisiesto) amb la finalitat de
     * construir el nou objecte amb la data correcta. El mÃ xim nombre de dies
     * que podrem afegir serÃ  30 i no podrem afegir un nombre negatiu de dies.
     *
     * @return boolean
     */
    public Data afegir(int numDias) {
        if (numDias < 0 || numDias > 30) {
            return this;
        }

        Data nuevaFecha = this.clone();
        int diasMes = getDiesMes(nuevaFecha.getMes(), nuevaFecha.getAny());

        nuevaFecha.set(nuevaFecha.getDia() + numDias, nuevaFecha.getMes(), nuevaFecha.getAny());

        while (nuevaFecha.getDia() > diasMes) {
            nuevaFecha.set(nuevaFecha.getDia() - diasMes, nuevaFecha.getMes() + 1, nuevaFecha.getAny());
            diasMes = getDiesMes(nuevaFecha.getMes(), nuevaFecha.getAny());
            if (nuevaFecha.getMes() > 12) {
                nuevaFecha.set(1, 1, nuevaFecha.getAny() + 1);
                diasMes = getDiesMes(1, nuevaFecha.getAny());
            }
        }

        return nuevaFecha;
    }

    /**
     * Retorna un nou objecte de tipus data que representa la data resultant de
     * restar el nombre de dies passats com a argument a la data que representa
     * l'objecte actual. Haurem de tindre en compte els dies que tÃ© el mes
     * actual i si l'any Ã©s de traspÃ s (bisiesto) amb la finalitat de construir
     * el nou objecte amb la data correcta. El mÃ xim nombre de dies que podrem
     * restar serÃ  30 i no podrem restar un nombre negatiu de dies.
     *
     * @return boolean
     */
    public Data restar(int numDias) {
        Data nuevaFecha = this.clone();

        int diasRestantes = numDias;
        while (diasRestantes > 0) {

            nuevaFecha.dia--;

            if (nuevaFecha.dia < 1) {
                nuevaFecha.mes--;
                if (nuevaFecha.mes < 1) {
                    nuevaFecha.any--;
                    nuevaFecha.mes = 12;
                }
                nuevaFecha.dia = getDiesMes(nuevaFecha.mes, nuevaFecha.any);
            }
            diasRestantes--;
        }

        return nuevaFecha;
    }

    /**
     * Retorna un booleÃ  indicant si la data representada per l'objecte actual
     * Ã©s correcta. No oblides comprovar que el dia es trobe dins del rang dels
     * dies que tÃ© el mes tenint en compte si l'any Ã©s de traspÃ s(bisiesto) o
     * no.
     *
     * @return
     */
    public boolean isCorrecta() {
        int maxDias = getDiesMes(this.mes, this.any);
        if (this.any > 0) {
            if (this.mes > 0 && this.mes < 13) {
                if (this.dia >= 1 && this.dia <= maxDias) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Retorna el mes del aÃ±o en formato text (enero, febrero, marzo,...)
     *
     * @return char
     */
    private String getMesEnFormatText() {
        return MESOS_TEXT[this.mes - 1];

    }

    /**
     * Devuelve el nÃºmero de dias transcurridos desde el 1-1-1
     *
     * @return int
     */
    private int getDiesTranscorregutsOrigen() {
        int total = 0;
        for (int i = 1; i < this.any; i++) {
            total += getDiesAny(i);
        }
        total += getDiesTranscorregutsEnAny();
        return total;
    }

    /**
     * Devuelve el nÃºmero de dias transcurridos en el anyo que representa el
     * objeto
     *
     * @return int
     */
    private int getDiesTranscorregutsEnAny() {
        int contador = 0;
        for (int i = 1; i < this.mes; i++) {
            contador += getDiesMes(i, this.any);
        }
        contador += this.dia;
        return contador;
    }

    /**
     * Indica si el aÃ±o pasado como argumento es bisiesto. Un aÃ±o es bisiesto si
     * es divisible por 4 pero no es divisible entre 100 o es divisible entre 4
     * entre 100 y entre 400
     *
     * @return boolean
     */
    public static boolean isBisiesto(int anyo) {
        return (anyo % 4 == 0 && anyo % 100 != 0) || (anyo % 4 == 0 && anyo % 100 == 0 && anyo % 400 == 0);
    }

    /**
     * Calcula el nÃºmero de dÃ­as que tiene el @mes en el @aÃ±o pasado como
     * argumento DeberÃ¡s hacer uso del mÃ©todos isBisiesto
     *
     * @return int total dias mes en curso
     */
    public static int getDiesMes(int mes, int anyo) {
        int dies = 0;
        switch (mes) {
            case 1 ->
                    dies = MESCON31;
            case 2 ->
                    dies = FEBREROB;
            case 3 ->
                    dies = isBisiesto(anyo) ? FEBREROB : FEBRERONOB;
            case 5 ->
                    dies = MESCON31;
            case 6 ->
                    dies = MESCON30;
            case 7 ->
                    dies = MESCON31;
            case 8 ->
                    dies = MESCON31;
            case 9 ->
                    dies = MESCON30;
            case 10 ->
                    dies = MESCON31;
            case 11 ->
                    dies = MESCON30;
            case 12 ->
                    dies = MESCON31;
        }
        return dies;
    }

    /**
     * Calcula el nÃºmero total de dias que tiene el aÃ±o pasado como argumento
     *
     * @return int total dias anyo en curso
     */
    public static int getDiesAny(int anyo) {
        boolean bisiesto = isBisiesto(anyo);
        int diesAny = 0;
        if (bisiesto) {
            diesAny = DIASBISIESTO;
        } else {
            diesAny = DIASNOBISIESTO;
        }
        return diesAny;
    }

    public int getAnysTranscorreguts(Data data) {
        int anysTrans = 0;
        anysTrans = data.any - this.any - 1;
        if ((data.mes > this.mes) || (data.dia > this.dia && data.mes >= this.mes)) {
            anysTrans++;
        }
        System.out.println(anysTrans);
        return anysTrans;
    }

    public int getminuts(int segons){
        int minuts = 0;
        minuts = segons / 60;
        return minuts;
    }
}
